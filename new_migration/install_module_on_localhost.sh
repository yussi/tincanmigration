#!/bin/bash
if [ $# -ne 2 ]; then 
    echo "usage: $0 <dump_path> <site_root_directory>"
    exit 1
fi
DATA_DIR=$1
UPLOAD_DIR=/tmp/migration_files
SITE_ROOT=$2

mkdir -p $UPLOAD_DIR
cp -r $DATA_DIR/images $UPLOAD_DIR
cp -r $DATA_DIR/downloads $UPLOAD_DIR
sudo cp -r drupal_migrate_module $SITE_ROOT/sites/all/modules/
sudo cp $DATA_DIR/images/image.php $SITE_ROOT
sudo cp $DATA_DIR/downloads/download.php $SITE_ROOT

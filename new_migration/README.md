Utils for migrating www.corporatewatch.org.uk from tincan's webbler CMS to the drupal CMS without access to their database.

The idea:
We fetch the pages, create a database, use that database and a custom drupal migrate module and relink old refrences.



fetch.lid.sh - this runs three scrips, fetch.indices.sh, fetch.downloads.sh and fetch.images.sh and creates a ./dumps/DATE/ directory, and passes this as the <PATH> to the other fetches. after running inspect the various log and fail files to ensure you haven't missed anything.

fetch.indices.sh <PATH> - fetches www.corporatewatch.org/index.php?lid=[1-10000] and places those under <PATH>/indices/, along with a 'log' and a 'fail' files for inspection. if already has a file, skip it. takes a long (long) time to run.

fetch.downloads.sh <PATH> - fetches www.corporatewatch.org/download.php?lid=[1-3000] and places those under <PATH>/downloads/, along with a 'log' and a 'fail' files for inspection. also generates a replacement download.php which redirects the old urls to the new location in drupal. this will try and redownload everything each time it runs.

fetch.images.sh <PATH> - fetches www.corporatewatch.org/image.php?lid=[1-3000] and places those under <PATH>/images/, along with a 'log' and a 'fail' files for inspection. if already has a file, skip it. takes a long (long) time to run. renames image from image.php?lid=NNNN to imageNNNN. also generates a replacement image.php which redirects the old urls to the new location in drupal.



build.py - this builds an sqlite database with the following tables: article,image,download to be used by the custom migrate module. 

for indices, fix links to all be relative, extracts and classify type of document ('page','article','newsitem' only for now). basic formatting, and use this to populate the table 'article'.

the tables download and image just contain a list of filenames.

drupal_migrate_module/ is the module for drupal.

The basic workflow is this.
cd tincanmigration/new_migration
$./fetch.lid.sh
inspect the log and fail files to make use under dump/DATE/indices,images,downloads
if you are missing files, you can get them individually or rerun the fetch.*.sh with the path including the date
When you thing you have everything you need, run
$./build.py -i ./dumps/DATE/ > build.log 2>&1 &
$tail -f build.log
this will try and build a databse, if some enteries in the database fail, it should say so in build.log.
when this is done, you'd have a ./peewee.db, which is the sqlite database. now run
$install_database_on_localhost.sh ./peewee.db <drupal mysql database name> <mysql user>
$install_module_on_localhost.sh ./dumps/DATE/ <site root>
where <site_root> is the root directory of the site such as /var/www/,/var/www/cw/,~/public_html/, or where ever your site sites on the filesystem

after that navigate to add content -> migrate in the drupal interface or using drush and preform the migration

#!/usr/bin/env python3.2
# coding: latin1
#python3.2

import codecs

import os
import sys
import traceback
import getopt;
from datetime import datetime
import re
from bs4 import BeautifulSoup
from peewee import *
#This creates an sqlite database from a site dump with index.php? files
#The created table is called Article 
#Original_Filename,Title,Heading, NavPath,Type,Content
# It looks for <div class='mainContent'> to populate the Content field
#
#Todo implement OldRef field by using the ?lid/?bid part of the OriginalFilename field to use for preserving old refrences
#
#


DumpPath="./corporatewatch.org/"
ArticleFileNameMatch=r'index.php\?lid=[1-9]+'
DownloadFileNameMatch=r'download.php\?id=[1-9]+'
ImageFileNameMatch=r'image.[1-9]+\..*'
OutputFile='corpwatch_tmp.db'
Verbose=False;

database=None;
class Article(Model):
    lid=IntegerField()
    Original_Filename=CharField()
    Title=CharField()
    Heading=CharField(null=True)
    NavPath=CharField(null=True) #We will want to disect this to figure out where the new location of the page should be
    Type=CharField(null=True) #News Item, Page,
    Content=TextField()
    OldRef=CharField(null=True)  #The ?lid=nnnn or ?bid=nnnn from Original_Filename Redundent, now in lid
    Tags=CharField(null=True) #, Seperated tags
    ModificationDate=DateField(null=True) #Set this later using set_dates.py by using http headers
    PublishDate=DateField(null=True) #Will only have date on 'news items' which are types of article, 
                                     #will take it from title, not enough info for the rest.
                                     #map to both created and modified dates.
    


    class Meta:
        database=database

class Box(Model):
    Original_Filename=CharField()
    Title=CharField(null=True)
    Content=CharField(null=True)
    Description=CharField(null=True)
    OnPages=CharField(null=True)
    Region=CharField(null=True)

    class Meta:
        database=database

class Download(Model):
    #lid=IntegerField(null=True)
    Original_Filename=CharField()
    class Meta:
        database=database


class Image(Model):
    #lid=IntegerField(null=True)
    Original_Filename=CharField()
    class Meta:
        database=database




class BuildArticleTable():


    #Returns (date(NONE),title,tags(""),"magazine/newsletter"
    def parseNewsLetter(self,title):
        doc_type="newsletter"
        #print(title)
        if (str(title).startswith("Magazine") or str(title).startswith("Corporate Watch : Magazine")):
            doc_type="magazine"
        return (None,title,"",doc_type)

    def parse(self,title):
        pass    


    #Checks that title starts with "M dd, YYYY :" and returns (title,date,tags) with the date removed from title
    #Assuming if this returns something then this is a 'newsitem'
    #TODO some news items have date in title in different format
    #(eg "January 8th, 2014" or "Corporate Watch : January 24, 2007","Palestine March 2013") 
    #needs to parse those too as newsitems, atm returns None for those
    #TODO Add tags
    #TODO Magazine/News letter/... (Only news items so far).
    def parseNewsItem(self,title):
        tags=""

        #In retrospect i could of done this much cleaner...
        match1=r'^\w* \d{1,2}\, \d{4} :*.*' #news format 1 - "M dd, YYY :"
        match2=r'^Palestine \w* \d{4}' #TODO set date to Year/Month, tag Palestine        
        match3=r'^\w* \d{1,2}(?:st|nd|rd|th), \d{4}' #January 8th, 2014
        match4=r'^Corporate Watch : \w* \d{1,2}, \d{4}' #Corporate Watch : January 24, 2007
        match5=r'^\w* \d{1,2}(?:st|nd|rd|th) \d{4}  :' #January 8th 2014  :
        match6=r'^Corporate Watch : \w* \d{1,2}(?:st|nd|rd|th)[,]? \d{4}[ ]{1,2}: ' #Corporate Watch : June 9th, 2010 : 
        match7=r'^Corporate Watch : Palestine \w* \d{4} : ' #Corporate Watch : Palestine April 2013 : 
        match8=r'^LATEST NEWS : ' #LATEST NEWS : 
        matcher = re.compile(match1)
        if matcher.match(title):
            #print("Matched match1..."+title)
            try:
                date_str=title.split(':')[0].strip()
                date=datetime.strptime(date_str,"%B %d, %Y")
                new_title=title.replace(date_str+" : ","")
                #print("looks like a news item:"+new_title+" "+date_str)
                return (date,new_title,"");
            except ValueError:
                print ("Not a date:"+title)
                print (ValueError)
                return None;
        #else: print ("Not matched "+title)
        matcher = re.compile(match2)
        if matcher.match(title): #Palestine March 2013 / Palestine Mar 2013
            #print("Matched Palestine..."+title)
            tags=tags+"Palestine,"
            try:
                date_str=title.split(':')[0].strip().replace("Palestine ","")
                month=date_str.split(" ")[0][0:3]
                year=date_str.split(" ")[1]
                date=datetime.strptime(month+" "+year,"%b %Y")
                new_title=title.split(":",1)[1].strip()
                return (date,new_title,tags);
            except ValueError:
                print ("Not a date:"+title)
                return None;
                
        matcher = re.compile(match3)
        if matcher.match(title):
            #print("Matched match3..."+title)
            try:
                date_str=title.split(':')[0].strip()
                month=date_str.split(" ")[0][0:3]
                day=date_str.split(" ")[1].replace("st,","").replace("nd,","").replace("rd,","").replace("th,","")
                year=date_str.split(" ")[2]
                new_title=title.replace(date_str+" :","").strip();
                #print(day+" "+month+" "+year+":"+new_title)
                date=datetime.strptime(day+" "+month+" "+year,"%d %b %Y")
                return (date,new_title,tags);
            except ValueError:
                print ("Not a date:"+title)
                return None;
        matcher = re.compile(match4)#Corporate Watch : July 27, 2012 :
        if matcher.match(title):
            #print("Matched match4..."+title)
            try:
                date_str=title.split(':')[1].strip()
                date=datetime.strptime(date_str,"%B %d, %Y")
                new_title=title.replace("Corporate Watch : "+date_str+" :","").strip();
                #print(new_title)
                return (date,new_title,tags);
            except ValueError:
                print ("Not a date:"+title)
                return None;
        matcher = re.compile(match5)
        if matcher.match(title):
            try:
                #print("Matched match5 "+title)
                date_str=title.split(':')[0].strip()
                month=date_str.split(" ")[0][0:3]
                day=date_str.split(" ")[1].replace("st","").replace("nd","").replace("rd","").replace("th","")
                year=date_str.split(" ")[2]
                new_title=title.replace(date_str+" :","").strip();
                #print(day+" "+month+" "+year+":"+new_title)
                date=datetime.strptime(day+" "+month+" "+year,"%d %b %Y")
                return (date,new_title,tags);
            except ValueError:
                print ("Not a date:"+title)
                return None;

        matcher = re.compile(match6)
        if matcher.match(title): #Corporate Watch : June 9th, 2010 : 
            try:
                #print ("Matched match6 "+title)
                date_str=title.split(" : ")[1].strip()
                #print (date_str)
                month=date_str.split(" ")[0][0:3]
                day=date_str.split(" ")[1].replace("st","").replace("nd","").replace("rd","").replace("th","").replace(",","")
                year=date_str.split(" ")[2]
                #print (day+"/"+month+"/"+year)
                new_title=title.replace("Corporate Watch : "+date_str+" : ","")
                date=datetime.strptime(day+" "+month+" "+year,"%d %b %Y")
                #print("new title:"+new_title+" date:"+str(date))
                return (date,new_title,tags);
            except ValueError:
                print ("Not a date:"+title)
                return None;

        matcher = re.compile(match7)
        if matcher.match(title): #Corporate Watch : Palestine April 2013 : 
            try:
                #print ("Matched match7 "+title)
                date_str=title.split(" : ")[1].strip()
                #print (date_str)
                month=date_str.split(" ")[1][0:3]
                year=date_str.split(" ")[2]
                #print (day+"/"+month+"/"+year)
                new_title=title.replace("Corporate Watch : "+date_str+" : ","")
                date=datetime.strptime(month+" "+year,"%b %Y")
                #print("new title:"+new_title+" date:"+str(date))
                return (date,new_title,tags);
            except ValueError:
                print ("Not a date:"+title)
                return None;
        matcher = re.compile(match8)
        if matcher.match(title): #Corporate Watch : Palestine April 2013 : 
            try:
                #print ("Matched match8 "+title)
                new_title=title.replace("LATEST NEWS : ","")
                date=None
                #print("new title:"+new_title+" date:"+str(date))
                return (date,new_title,tags);
            except ValueError:
                print ("Not a date:"+title)
                return None;
        print ("RETURNING NULL, NOT GOOD NEWS ITEM HERE!!!\n"+title)
        return None;
        
    #Takes a filename 'index.html?lid=NNNN.html' and returns 'lid=NNNN' 
    #@DEPRECATED
    def getOldRefFromFilename(self,fn):
        s=fn.replace("index.html?","").replace(".html","")
        #print (s)
        return s;
    #Takes a filename 'index.html?lid=NNNN.html' and returns 'NNNN'
    def getLidFromFilename(self,fn):
        s=fn.replace("index.php?lid=","").replace(".php","")
        #print (s)
        return s;
    
    #put the stuff we want from document in a Document class
    #Takes in a soup and a filename
    #Returns a an Article
    def classifyDocument(self,soup,fn):
        try:
            contents=soup.find('div',{'class':'mainContent'}) #Here need to get rid of navpath part
            #contents=self.fixHideNavPath(contents)
            title=soup.find('title').string #Here take inside text only
            navpath=soup.find('span',{'class','topboxtext'})
            heading=soup.find('span',{'class','mainColHd'}) #That's actually wrong, not the heading part but the whole page!
            doc_type=""
            tags="corporate watch," #Just a default value, breaks sql/convert step if empty...
            old_ref=self.getOldRefFromFilename(fn); #@DEPRECATED
            lid=self.getLidFromFilename(fn);
            date=None; #Only news items have dates :P
            #Make sure page has <div class='mainContent'>
            if contents is None:
                print("file "+fn+" has no contents")            
                return
            #TODO
            #Remove tag <span class='topboxtext'>, ie remove the navpath
            
            #For nav path logic
            if (navpath is not None):
                home=(navpath.find_all('a')[0].text)
                first_level=navpath.find_all('a')[1].text
            else: 
                print ("page "+lid+" has no navpath ")
            
            #Do some logic to figure out what kind of document we're dealing with (article,page,newsitem)
            #All of this can be done much cleaner...
            #TODO finish newsitem, 
            #TODO magazine, company profile, etc
            #print(home+" -> "+first_level)
            if (navpath is None):
                doc_type="page"
            elif (first_level=='LATEST NEWS'):
                if (Verbose):print ("Latest news: "+title)
                (date,title,tags)=self.parseNewsItem(title)
                doc_type="news"
                tags=tags+"news," 
            elif (first_level=='NEWSLETTERS'):
                #print ("Newsletter: "+title)
                if (str(title).startswith("Magazine") or str(title).startswith("Corporate Watch : Magazine")):
                    doc_type="magazine"
                else: doc_type="newsletter"
                #(date,title,tags,doc_type)=self.parseNewsLetter(title)
            elif (first_level=='About CWatch'):
                #print ("About page")
                doc_type="aboutpage"
            elif (first_level in ("PROFILES OVERVIEW","ARMAMENTS","BIOTECH","CHEMICALS","CONSTRUCTION",\
                                  "FOOD & AGRICULTURE","OTHER","PRIVATISED SERVICES",\
                                  "OIL / GAS","PALESTINE RESEARCH","PHARMACEUTICALS","PUBLIC RELATIONS")):
                #print("corporate profile")
                doc_type="companyprofile"
            elif (first_level in ("RESEARCH OVERVIEW","Aid & Development","CORPORATE STRUCTURES RESEARCH",\
                                  "PROJECTS3","STATE/CORPORATE CRACKDOWN","FOOD & AGRICULTURE RESEARCH",\
                                  "IRAQ","MIGRATION","PALESTINE RESEARCH","PRIVATISATION","PUBLIC RELATIONS RESEARCH",\
                                  "STATE/CORPORATE CRACKDOWN")):
                #print("research")
                doc_type="research"
            elif (first_level == "REPORTS"):
                #print("report:"+title)
                doc_type="report"
            elif (first_level == "RESOURCES"):
                #print("resource:"+title)
                doc_type="resource"
            elif (navpath is not None):
                #print ("article:"+title)
                doc_type="article"
                #tags=tags+",article" 
            else:
                #print ("This is not a news item, now it's a page: "+title)
                doc_type="page"
                #tags=tags+",page" 
        except Exception as e:
            print ("BeautifulSoup error on file : "+fn)
            print (e)
            ex_type, ex, tb = sys.exc_info()
            traceback.print_tb(tb)
        try:
            tags=str(tags).strip(',')
            art=Article.get(Article.lid == lid)
            #s, autogenerated by migration
            print ("Found existing record, doing nothing to "+lid)
        except Article.DoesNotExist:
            #The following disaster is me trying to figure out how to put this into utf-8, in order to
            #elim. those pesky escape chars, but i gave up and just string replaced some of those, TODO &nbsp char
            #leint("creating "+lid)
            #c=contents.decode('ISO-8859-1').encode('utf8')
            #c=str(contents).decode('iso-8859-1').encode('utf8')
            #c=str(contents).replace("\u0094","\"").replace("\u0093","\"").replace("\u0092","\'").replace("\u0091","\'")
            contents=self.fixHideNavPath(contents)
            c=str(contents).replace("\u0094","&#x94;").\
                        replace("\u0093","&#x93;").\
                        replace("\u0092","&#x92;").\
                        replace("\u0091","&#x91;").\
                        replace("\u0096","&#x96;").\
                        replace("\u0085","&#x85;").\
                        replace("\u0095","&#x95;")

            #c=str(contents)
            #c=str(c).encode("utf-8")
            #c=contents.prettify().decode('iso-8859-1').encode('utf8')
            #c=contents.decode("latin1")
            #print(c);
            #d=contents.prettify().decode("iso-8859-1").encode("utf-8")

            #print(type(c))
            #print("Soup encoding:"+soup.originalEncoding());
            #print(contents);
            art=Article.create(
                lid=int(lid),
                Original_Filename=str(fn),
                Title=str(title),
                Heading=str(heading),
                NavPath=str(navpath),
                #REF:http://stackoverflow.com/questions/1155903/latin-1-and-the-unicode-factory-in-python
                #http://stackoverflow.com/questions/3942888/unicodeencodeerror-latin-1-codec-cant-encode-character
                #Content=contents.encode_contents(),
                #Content=contents.decode('cp1252').encode('utf-8'),
                #Content=contents.encode('cp1252').decode('latin-1'),
                #Content=bytes(str(contents),"cp1252").decode("latin-1"),
                #Content=bytes(str(contents),"latin-1").decode("unicode_escape"),
                #Content=codecs.encode(str(contents)),
                #Content=contents.encode('UTF-8'),
                #Content=contents.decode("latin-1").encode("utf-8"),
                #Content=str(contents),
                Content=c,
                Type=str(doc_type),
                OldRef=str(old_ref),
                Tags=tags,
                PublishDate=date
                )
                
        except Exception as e:
            print("Record create error for file : "+fn)
            print(e)
        return art

    #Just grab a soup from a filename (with path)
    def getSoup(self,fn):
        #print (fn)
        #Needs encoding cause dump files seem to be in wierd format and not ascii as they claim
        #with open(fn, encoding='ISO-8859-1', mode='r') as content_file:
        with open(fn, encoding='ISO-8859-1', mode='r') as content_file:
            fcontent=content_file.read()
            soup = BeautifulSoup(fcontent)
            #soup = BeautifulSoup(fcontent, from_encoding="ISO-8859-1")
            #soup = BeautifulSoup(fcontent, from_encoding="ISO-8859-1", convert_entities=BeautifulSoup.HTML_ENTITIES)
            return soup
            #print fcontent

    def dumpDB(self):
        for article in Article.select(): 
            print (article.Title)
    def fixHideNavPath(self,soup):
        ret=soup
        [s.extract() for s in ret.findAll(attrs={"class":"topboxtext"})]
        return ret
        
    def fixChars(self,contents):
        pass        

    def fixLinks(self,soup):
        for a in soup.findAll('a'):
            if a.has_key('href'):
                a['href']=a['href'].\
                    replace("http://www.corporatewatch.org.uk/","").\
                    replace("http://www.corporatewatch.org/","").\
                    replace("http://corporatewatch.org/","").\
                    replace("http://corporatewatch.org/.uk","")
                    #replace("/?lid=","?lid=").\
                    #replace("/?bid=","?bid=").\
                    #replace("/download.php?id=","download.php?id=").\
                    #replace("/image.php?id=","image.php?id=")
                a['href']=re.sub("^/","",a['href'])
                if (Verbose==True):print("a href:"+a['href']);
        for i in soup.findAll('img'):
            if i.has_key('src'):
                i['src']=re.sub("^/","",i['src'])
                if (Verbose==True):print("img src:"+i['src'])
                
        ret=soup
        return ret;    

    def buildBlocks(self,soup,art):
        LeftColumn=soup.find('div',{'class','leftCol'})
        RightColumn=soup.find('div',{'class','rightCol'})
        for a in LeftColumn.findAll('span'):
            print(a.get("class"));
        #print (""+str(art.lid)+" "+art.Type)
        #print (LeftColumn);
        pass

    def __init__(self):
        pth=DumpPath+"/indices/";
        match=ArticleFileNameMatch;
        matcher = re.compile(match)
        Article.create_table(True)
        Box.create_table(True)
        for fn in os.listdir(pth):
            if matcher.match(fn): 
                if(Verbose==True):print("filename:"+fn)
                soup=self.getSoup(pth+str(fn))
                soup=self.fixLinks(soup)
                art=self.classifyDocument(soup,str(fn) )
                #if (art is not None):self.buildBlocks(soup,art)
class BuildDownloadTable():
    def add_file(self,fn):
        #lid=fn.split('?id=')[1]
        try:
           safe_fn=str(fn).replace("\udc92","") #This is microsoft's (user) fault!
           #print(safe_fn)
           dow=Download.get(Download.Original_Filename==str(safe_fn))
        except Download.DoesNotExist:
            Download.create(Original_Filename=str(safe_fn))
        except Exception as e:
            print("Something bad happened when adding a record for "+safe_fn)
            print(e)
            sys.exit(-1)
    def __init__(self):
        pth=DumpPath+"/downloads/";
        match=DownloadFileNameMatch;
        matcher = re.compile(match)
        Download.create_table(True)
        for fn in os.listdir(pth):
            if not matcher.match(fn): 
                self.add_file(fn)
    
class BuildImageTable():
    def add_file(self,fn):
        #lid=fn.split('?id=')[1]
        try:
           img=Image.get(Image.Original_Filename == str(fn))
        except Image.DoesNotExist:
            Image.create(Original_Filename=str(fn))
        except Exception as e:
            print("Something bad happened when adding a record for "+fn)
            print(e)
            sys.exit(-1)

    def __init__(self):
        pth=DumpPath+"/images/";
        match=ImageFileNameMatch;
        matcher = re.compile(match)
        Image.create_table(True)
        for fn in os.listdir(pth):
            if (Verbose==True):print ("filename:"+fn)
            if matcher.match(fn): 
                self.add_file(fn)
                print(fn)

if __name__=="__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:],"i:o:v",["input=","output="]) #Not sure what everything here does
        #opts, args = getopt.getopt(sys.argv[1:],"io:v",["input","output"]) #Not sure what everything here does
        for o,a in opts:
            if (o=="-i" or o=="--input"):
            #if o in ('-i','--input'):
                print ("input"+a)
                DumpPath=a
            elif o in ('-m','--match'):
                print ("match"+a)
                ArticleFileNameMatch=a
            elif o in ('-o','--output'):
                print ("output"+a)
                OutputFile=a
            elif o in ('-v','--verbose'):
                Verbose=True;
            else: pass
    except getopt.GetoptError:
        print ("Usage %s <-i Input direcotry> \
                <-o Output sqlite file>  \
                [-v Verbose output (very verbose)]  \
                [-m Regular expression to match filename]",
                sys.argv[1])
        sys.exit();
    try:
        database = SqliteDatabase(OutputFile)
        database.connect()
    except:
        print("Database connect error")
        sys.exit(-1)
    art_table=BuildArticleTable()
    dow_table=BuildDownloadTable()
    img_table=BuildImageTable()
#ref http://stackoverflow.com/questions/6798097/find-regex-in-python-or-how-to-find-files-whose-whole-name-path-name

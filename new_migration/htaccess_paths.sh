#!/bin/bash
#This attempts to generate the old refrences from tincan(bid/lid) to paste into .htacess redirects [301]
#This needs to run on a server AFTER the migration completed.
#Depends on tables migrate_map_article, article tables
#

#migrate_map_article table
SELECT_MMA="select sourceid1, destid1 from migrate_map_article;"
SELECT_OLDREF="select OldRef from article where id=%b;"
echo "Select MMA:$SELECT_MMA"


DB_NAME="drupal"
DOMAIN_ROOT="mishagale.co.uk/~uri/drupal/"

MYSQL_LOGIN="uri"
MYSQL_HOST="localhost"

TMP_MDBM="/tmp/migration_db_mapping" #A list of (src_id, dest_id) which maps drupal node id to  article id
TMP_OLDREF="/tmp/migration_old_ref" #A list of old ref (bid=123 or lid=123)

Query="use $DB_NAME;$SELECT_MMA"

#Super secure but hey...
echo  -n "MySql Password:";
read -s MYSQL_PASS;


#ENABLE THIS IN PRODUCTION!!!
echo $Query | mysql --skip-column-names --host=$MYSQL_HOST --user=$MYSQL_LOGIN --password=$MYSQL_PASS > $TMP_MDBM

echo "#Old Refrences redirect" > htaccess_rules
while read p; do
    SRC=$(echo -n $p|cut -f1 -d" ")
    DST=$(echo -n $p|cut -f2 -d" ")
    QueryOldRef=$(printf "$SELECT_OLDREF" $SRC) 
    Query="use $DB_NAME;$QueryOldRef"
    echo "Query $Query"
    OldRef=`echo  $Query | mysql --skip-column-names --host=$MYSQL_HOST --user=$MYSQL_LOGIN --password=$MYSQL_PASS|xargs echo -n` #THIS IS NOT WORKING!
    echo "OldRef:$OldRef"
    #echo "SELECT_OldRef: $Query"
    echo "src=${SRC}; map oldref=${OldRef} to node id=${DST}"
    echo "RewriteCond %{QUERY_STRING} ^${OldRef}$" >> htaccess_rules
    echo "RewriteRule ^.*$ ${DOMAIN_ROOT}?q=node/${DST} [R=301,L]" >> htaccess_rules
    echo "" >> htaccess_rules

    echo "---- look a long and pointless seperator line------"
done < $TMP_MDBM

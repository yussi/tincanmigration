#!/bin/bash
#Fetches corporatewatch's images by id from 1 to 300 ($LAST_ID)
#@argument 1 root directory of downloaded files($OUT_DIR), will store under $OUT_DIR/images
#generates an image.php file to map each file to an id for legacy url mapping under same location
#generates log,fail files

OUT_DIR="$1/indices/"
LOG_FILE="log"
FAIL_FILE="fail"
LAST_ID=10000
mkdir -p $OUT_DIR
cd $OUT_DIR #This annoyance comes from the --remote-name in curl
#for i in `seq 4996 4999`; do
for i in `seq 1 $LAST_ID`; do
    if [[ -f "index.php?lid=$i" ]]; then
        echo `date +%F.%T`" - Already got index.php?lid=$i, continuing" >> $LOG_FILE
        echo "Already got index.php?lid=$i, continuing"
        continue
    fi
    echo -ne "$i / $LAST_ID \r"
    stat=`curl -P $OUT_DIR --retry 10 --remote-header-name --remote-name --write-out "%{http_code};%{filename_effective};%{content_type};%{redirect_url}\n\n" --silent www.corporatewatch.org/index.php\?lid=$i`
    http_resp_code=`echo $stat| cut -d \;  -f 1`
    http_file_name=`echo $stat| cut -d \;  -f 2`
    http_cont_type=`echo $stat| cut -d \;  -f 3`
    http_addr_redi=`echo $stat| cut -d \;  -f 4`
    #echo $stat
    #echo $http_file_name
    if [ $http_resp_code -eq 404 ]; then
        #echo "no document at $http_file_name (404)"
        echo -ne "$i / $LAST_ID - $http_file_name not found (404) \r"
        echo `date +%F.%T`" - $http_file_name not found (404)" >> $LOG_FILE
        rm "$http_file_name";
        continue;
    elif [ $http_resp_code -eq 302 ]; then #This doesn't always catch for some reason, not very relevant to me now
        echo `date +%F.%T`" - $http_file_name redirects to $http_addr_redi ($http_cont_type)\n\r"
        echo `date +%F.%T`" - $http_file_name redirects to $http_addr_redi ($http_cont_type)" >> $LOG_FILE
        echo `date +%F.%T`" - $http_file_name" >> $FAIL_FILE
    elif [ $http_resp_code -ne 200 ]; then
        echo "ERROR: HTTP response code $http_resp_code for file $http_file_name ($http_cont_type)"
        echo `date +%F.%T`" - ERROR: HTTP response code $http_resp_code for file $http_file_name ($http_cont_type)" >> $LOG_FILE
        echo `date +%F.%T`" - $http_file_name" >> $FAIL_FILE
        #rm $http_file_name
        #continue;
        #exit -1
    fi
    if [[ $http_cont_type != text/html* ]]; then
        echo "$i / $LAST_ID - Warnning $http_file_name not a text/html \($http_cont_type)\n\r"
        echo `date +%F.%T`" - WARNNING: file $http_file_name  not text/html ($http_cont_type)" >> $LOG_FILE
    fi

    file_size=$(du -b "$http_file_name" | cut -f 1)
    echo $file_size
    if [ $file_size -le 1 ]; then #Sometimes 302 redirects fetch a file with size 1 byte
        echo "$i / $LAST_ID - empty file \n\r"
        echo `date +%F.%T`" - empty file $http_file_name" >> $LOG_FILE
        echo `date +%F.%T`" - $http_file_name" >> $FAIL_FILE
        rm $http_file_name
        continue;
    elif [ $file_size -eq 240 ]; then #this throws an exception, also this shouldn't happen as his is a 404 page 
        echo "$i / $LAST_ID - file size 240 bytes, looks like a 404 removing \n\r"
        echo `date +%F.%T`" - file size 240 bytes, looks like a 404 removing $http_file_name" >> $LOG_FILE
        echo `date +%F.%T`" - $http_file_name" >> $FAIL_FILE
        rm "$http_file_name"
        continue
    else
        echo -ne "$i / $LAST_ID - $http_file_name \r"
        echo `date +%F.%T`" - index lid $i -> $http_file_name" >> $LOG_FILE
    fi
done
cd -

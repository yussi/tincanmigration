#!/usr/bin/env python3.2
import urllib3;
import httplib2;
import sys;
from datetime import datetime
from peewee import *
import getopt;

db=None;
class Article(Model):
    lid=IntegerField()
    Original_Filename=CharField()
    Title=CharField()
    Heading=CharField(null=True)
    NavPath=CharField(null=True) #We will want to disect this to figure out where the new location of the page should be
    Type=CharField(null=True) #News Item, Page,
    Content=TextField()
    OldRef=CharField(null=True)  #The ?lid=nnnn or ?bid=nnnn from Original_Filename Redundent, now in lid
    Tags=CharField(null=True) #, Seperated tags
    ModificationDate=DateField(null=True) #Set this later using set_dates.py by using http headers
    PublishDate=DateField(null=True)
    class Meta:
        database=db


def set_database(fn):
    global db
    try:
        db=SqliteDatabase(fn)
        db.connect()
    except Exception as e:
        print(e)
        exit(-1);

def get_response_modified_date(lid):
    h = httplib2.Http(".cache")
    resp, content = h.request("http://www.corporatewatch.org/?lid="+str(lid), "__GET__")
    date_str=resp['last-modified']
    ret=datetime.strptime(date_str, "%a, %d %b %Y %H:%M:%S %z")
    print(ret);
    return ret;
    '''
    conn=urllib3.connectionpool.connection_from_url('http://www.corporatewatch.org/?lid='+str(lid))
    r=conn.request('GET','/')
    print(r.headers)

    if (not r.headers['last-modified']):
        print("No last modified date")
        #raise Exception("No last modified in header of lid="+str(lid))
        return None;
    else:
        try:
            date_str=r.headers['Last-Modified']
            print("date_str:"+date_str)
            #'Thu, 06 Mar 2014 14:37:51 +0000'
            ret=datetime.strptime(date_str, "%a, %d %b %Y %H:%M:%S +0000")
            #print(ret)
            return ret;
        except ValueError as ve:
            print(ve);
            return ret;
        except Exception as e:
            print("Date error something")
            print(e)
'''
if __name__=="__main__":
    #Parse arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:],"i:o:v",["input=","output="]) #Not sure what everything here does
        for o,a in opts:
            if (o=="-i" or o=="--input"):
                print ("Using file"+a)
                set_database(a)
    except getopt.GetoptError as oe:
        print(oe)
#    d=get_response_modified_date(3833)
    #Iterate over lids
    for art in Article.select():
        print(art.lid);
        #Get date
        try:
            if (art.ModificationDate is not None):
                print("Already got a date for lid="+str(art.lid)+" as in "+str(art.ModificationDate))
                if (art.PublishDate is None or art.PublishDate==art.ModificationDate):
                    print("Changing PublishDate")
                    new_date=art.ModificationDate
                    print("NEWDATE: "+new_date)
                    update_query=Article.update(PublishDate=art.ModificationDate).where(Article.lid==art.lid)
                    update_query.execute()
                continue
            d=get_response_modified_date(art.lid)
            if (d is None): 
                print("No date")
                continue;
            print("d:"+str(d))
            update_query=Article.update(ModificationDate=d).where(Article.lid==art.lid)
            update_query.execute()
            print(update_query)
            #art.ModificationDate=d
        except Exception as e:
            print("Error getting date")
            print("err: "+str(e))

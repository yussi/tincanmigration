#!/bin/bash
#Fetches corporatewatch's downloads by id from 1 to 300
#@argument 1 root directory of downloaded files(OUT_DIR), will store under $OUT_DIR/downloads
#generates a download.php file to map each file to an id for legacy url mapping under same location
#

OUT_DIR="$1/downloads/"
PHP_FILE="download.php"
LOG_FILE="log"
FAIL_FILE="fail"
LAST_ID=3000
mkdir -p $OUT_DIR
cd $OUT_DIR #This annoyance comes from the --remote-name in curl
echo "<?php " > $PHP_FILE
echo "#For legacy url refrences, autogenerated by migration">> $PHP_FILE
echo "\$id=\$_GET['id'];" >> $PHP_FILE
#for i in `seq 1 3`; do
for i in `seq 1 $LAST_ID`; do




    stat=`curl --retry 10 --remote-header-name --remote-name  http://www.corporatewatch.org/download.php?id=$i --silent --write-out "%{http_code};%{filename_effective};%{content_type}\n\n"`
    #stat=`curl -P $OUT_DIR --retry 10 --remote-header-name --remote-name --write-out "%{http_code};%{filename_effective};%{content_type}\n\n" --silent www.corporatewatch.org/download.php?id=$i`
    http_resp_code=`echo $stat| cut -d \;  -f 1`
    http_file_name=`echo $stat| cut -d \;  -f 2`
    http_cont_type=`echo $stat| cut -d \;  -f 3`
    #echo $stat
    #echo $http_file_name
    if [ $http_resp_code -ne 200 ]; then
        echo "ERROR: HTTP response code $http_resp_code for download index $i "
        echo `date +%F.%T`" - ERROR: HTTP response code $http_resp_code for file index $i $http_file_name ($http_cont_type)" >> $LOG_FILE
        echo `date +%F.%T`" - download.php?id=$i" >> $FAIL_FILE
        continue;
    fi
    if [[ $http_file_name == download.php?id=* ]]; then #Assumes that if no human sensible filename then no such file exists
        echo "$i / $LAST_ID - $http_file_name is not a download ($http_cont_type)"
        echo `date +%F.%T`" - $http_file_name  does not point to a file ($http_cont_type)" >> $LOG_FILE
        rm $http_file_name
        continue
    else
        echo "$i / $LAST_ID - $http_file_name"
        echo "download id $i -> $http_file_name" >> $LOG_FILE
        echo "if (\$id==$i){header(\"Location: sites/default/files/$http_file_name\");die();};" >> $PHP_FILE
        #echo "$i $http_file_name" >> $PHP_FILE
    fi
done
echo "?> " >> $PHP_FILE
cd -

<?php
/*
    This is my first attempt at changing the beer.inc example from drupal migrate to do the corpwatch migration
*/
abstract class BaseMigration extends DynamicMigration {
    public function __construct(){
        parent::__construct();
        //I guess these are Drupal admins?
        //$this->team = array(#TODO
        //  new MigrateTeamMember('Liz Taster', 'ltaster@example.com', t('Product Owner')),
        //  new MigrateTeamMember('Larry Brewer', 'lbrewer@example.com', t('Implementor')),
        //);
        //Issue tracker
        //$this->issuePattern = 'http://drupal.org/node/:id:';
        }
}

class DownloadMigration extends BaseMigration{
    public function __construct(){
        parent::__construct();
        $this->description = t('A migration class for migrating downloads');
        $this->map = new MigrateSQLMap($this->machineName,
          array(
            'id' => array( //TODO maybe this needs to be 'OldRef => array?
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'description' => 'ID of temp migration table from tincan',
            ),
          ),
          MigrateDestinationNode::getKeySchema()
        );
        $query = db_select('download','dow')->fields('dow',array('id','Original_Filename'));
        $this->source = new MigrateSourceSQL($query);
        $this->destination = new MigrateDestinationFile('file','MigrateFileUri');
        $this->addFieldMapping('value','Original_Filename');
        $this->addFieldMapping('source_dir')->defaultValue('/tmp/migration_files/downloads');
         
    }
    #Called before, on source row.
    public function prepareRow($current_row) {}
    #Called on dest
    public function prepare(stdClass $node, stdClass $row) {}
    #Called right after prepare and save
    public function complete($entity, stdClass $row){}
}

class ImageMigration extends BaseMigration{
    public function __construct(){
        parent::__construct();
        $this->description = t('A migration class for migrating images');
        $this->map = new MigrateSQLMap($this->machineName,
          array(
            'id' => array( //TODO maybe this needs to be 'OldRef => array?
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'description' => 'ID of temp migration table from tincan',
            ),
          ),
          MigrateDestinationNode::getKeySchema()
        );
        $query = db_select('image','img')->fields('img',array('id','Original_Filename'));
        $this->source = new MigrateSourceSQL($query);
        #$this->destination = new MigrateDestinationMedia('image');
        $this->destination = new MigrateDestinationFile('file','MigrateFileUri');
        $this->addFieldMapping('value','Original_Filename');
        $this->addFieldMapping('field_image', 'Original_File');
        $this->addFieldMapping('source_dir')->defaultValue('/tmp/migration_files/images');
         
    }
    #Called before, on source row.
    public function prepareRow($current_row) {}
    #Called on dest
    public function prepare(stdClass $node, stdClass $row) {}
    #Called right after prepare and save
    public function complete($entity, stdClass $row){}
}

class ArticleMigration extends BaseMigration{
    public function __construct(){
        parent::__construct();
        $this->description = t('This is the tincan migration article');
        //TODO not sure what goes here ??!
        //I think this needs to map to id, am I right?
        $this->map = new MigrateSQLMap($this->machineName,
          array(
            'lid' => array( //TODO maybe this needs to be 'OldRef => array?
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'description' => 'ID of temp migration table from tincan',
            ),
          ),
          MigrateDestinationNode::getKeySchema()
        );
        $query = db_select('article','art')
                 ->fields('art',array(  'lid', 
                                        'Original_Filename',
                                        'Title',
                                        'Heading',
                                        'NavPath',
                                        'Type',
                                        'Content',
                                        'OldRef',
                                        'Tags',
                                        'PublishDate'));

        $this->source = new MigrateSourceSQL($query);
        //Not sure what goes here...
        //TODO check if this is a 'page' or 'article' and act accordingly
        $this->destination = new MigrateDestinationNode('article');
        //$this->addFieldMapping(TO, FROM);
        $this->addFieldMapping('title', 'Title');
        $this->addFieldMapping('type', 'Type');
        $this->addFieldMapping('body', 'Content')
                ->arguments(array('format' => 'full_html'));
        $this->addFieldMapping('created','PublishDate');
        $this->addFieldMapping('changed','ModificationDate');
        #This should map the old path... i think
        $this->addFieldMapping('oldref', 'OldRef'); #Not sure if that's allowed ?! (making my own fields...)
        #$this->addFieldMapping('path', 'my_legacy_alias_field');
        #Probably isn't required anymore since this is now done in mapping
        $this->addFieldMapping('nid', 'lid')
             ->description(t('Preserve old tincan lid as nid in Drupal')); 
        $this->addFieldMapping('is_new')
             ->defaultValue(TRUE);
        } 

    #Called before, on source row.
    public function prepareRow($current_row) {
    }
    #Called on dest
    public function prepare(stdClass $node, stdClass $row) {
        #TODO here, set up an alias or some other redirect from old url.
        #$node->path="?".$row->oldref;
    }
    #Called right after prepare and save
    public function complete($entity, stdClass $row){
        if ($row->lid){
            #path_set_alias('node/' . $entity->nid, '?lid='.$row->lid, NULL, 'en');
        }
    }
}
?>

<?php
function migrate_tin_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'tin' => array(
        'title' => t('Tin Migrations'),
      ),
    ),
    'migrations' => array(
      'Article' => array(
        'class_name' => 'ArticleMigration',
        'group_name' => 'tin',
      ),
      'Download' => array(
        'class_name' => 'DownloadMigration',
        'group_name' => 'tin',
      ),
      'Image' => array(
        'class_name' => 'ImageMigration',
        'group_name' => 'tin',
      ),
    ),
  );
  return $api;
}
?>

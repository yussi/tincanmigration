#!/bin/bash
# -*- coding: utf-8 -*-
if [ $# -ne 3 ]; then 
        echo "usage $0 <sqlite database file> <mysql drupal database> <mysql login>"
        exit 1 
fi
TMP_FILE=/tmp/create_article.sql
INPUT_SQLITE_DUMP=$1
OUTPUT_MYSQL_DATABASE=$2
MYSQL_LOGIN=$3
CONVERT_PATH=./convert.perl
sqlite3 $INPUT_SQLITE_DUMP .dump |$CONVERT_PATH |  sed '1s/.*/USE '${OUTPUT_MYSQL_DATABASE}';/' > $TMP_FILE
echo "mysql"
cat $TMP_FILE | mysql -v -v -v -u $MYSQL_LOGIN -p

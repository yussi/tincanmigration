#!/bin/bash
DOMAIN="corporatewatch.org"
ROOT="/"
LOG="./fetch.log"
OUT_DIR=./dumps/`date +%d-%B-%Y`
echo $OUT_DIR
mkdir -p $OUT_DIR

#Fetch downloads first
echo "Fetching downloads"
./fetch.downloads.sh $OUT_DIR

#Fetch images second
echo "Fetching images"
./fetch.images.sh $OUT_DIR

#Fetching indices
echo "Fetching indices"
./fetch.indices.sh $OUT_DIR

#OLD SHIT FOLLOWS BELOW
#Fetch images first
#wget --random-wait --directory-prefix=$OUT_DIR -p  --continue  --relative --domains='corporatewatch.org'   http://www.corporatewatch.org/image.php?id={0..2000}
#Then fetch downloads
#./fetch.downloads.sh $OUT_DIR
#wget --random-wait --directory-prefix=$OUT_DIR -p  --continue  --relative --domains='corporatewatch.org'   http://www.corporatewatch.org/download.php?id={0..500}


##Fetch lids
##http://www.corporatewatch.org/?lid=4947 tried to pull mappingthecorporations.org  which is down, 
#exclude-domains didn't work for this, hackish sol'n (if you can call it that)
#wget --timeout=300 --remote-encoding=ISO-8859-1 --local-encoding=utf-8 --convert-links --random-wait --directory-prefix=$OUT_DIR -p  --continue --save-headers --relative --domains='corporatewatch.org'  --exclude-domains='mappingthecorporations.org'   http://www.corporatewatch.org/?lid={{0..4996},{4998..8000}} 

#Finally, do a recursive fetch to get stuff we may have missed
#wget --tries=5 --timeout=120 --content-disposition --remote-encoding=ISO-8859-1 --local-encoding=utf-8 --convert-links --random-wait --directory-prefix=$OUT_DIR -p  -r --continue --save-headers  --domains='corporatewatch.org'  --exclude-domains='mappingthecorporations.org'  -e robots=off -U mozilla http://www.corporatewatch.org/

#Remove empties
#rm `find $OUT_DIR -size 18c -name "download.php\?id=*"`
#Empty 1x1 png
#rm `find $OUT_DIR -size 167c -name "image.php\?id=*"`

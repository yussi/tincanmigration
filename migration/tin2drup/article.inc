<?php
/*
    This is my first attempt at changing the beer.inc example from drupal migrate to do the corpwatch migration
*/
abstruct class BaseMigration extends DynamicMigration {
    public function __construct(){
        parent::__construct()
        //I guess these are Drupal admins?
        $this->team = array(#TODO
          new MigrateTeamMember('Liz Taster', 'ltaster@example.com', t('Product Owner')),
          new MigrateTeamMember('Larry Brewer', 'lbrewer@example.com', t('Implementor')),
        );
        //Issue tracker
        //$this->issuePattern = 'http://drupal.org/node/:id:';


    }
}

class ArticleMigration extends BaseMigration{
    public function __construct(){
        parent::__construct()
        this->description = t('This class is used to map from the sqlite 
                               article database i created from tincan to the drupal db');
    
    }
    //TODO not sure what goes here ??!
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'style' => array('type' => 'varchar',
                           'length' => 255,
                           'not null' => TRUE,
                           'description' => 'Topic ID',
                          )
        ),
        MigrateDestinationTerm::getKeySchema()
      );
    //TODO figure what goes where and who's against whom and why
    /*
     $query = db_select('migrate_example_beer_topic', 'met')
                 ->fields('met', array('style', 'details', 'style_parent', 'region', 'hoppiness'))
                 // This sort assures that parents are saved before children.
                 ->orderBy('style_parent', 'ASC');
    */
    $query = Database::getConnection('default', 'legacy') //TODO Figure out how to SQLITE here...
             ->select('article')
             ->fields(array('id', 'Original_Filename', 'Title','Heading','NavPath','Content'));

    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationTerm('migrate_item');
    
}
?>

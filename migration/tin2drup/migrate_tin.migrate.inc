<?php
function migrate_tin_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'tin' => array(
        'title' => t('Tin Migrations'),
      ),
    ),
    'migrations' => array(
 //     'TinUser' => array(
//        'class_name' => 'MigrateTinUserMigration',
//        'group_name' => 'tin',
//      ),
      'TinArticle' => array(
        'class_name' => 'MigrateTinArticleMigration',
        'group_name' => 'tin',
      ),
    ),
/*   Not sure if this goes here or what exactly it expects yet
    'destination handlers' => array(
      'ExampleNodeHandler',
    ),
    'field handlers' => array(
      'ExampleCustomFieldHandler',
    )
*/
  );
  return $api;
}
?>

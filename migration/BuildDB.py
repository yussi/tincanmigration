#!/usr/bin/python3.2
# -*- coding: gb2312 -*-
#python3.2

#I did this twice, once before and once after i found out about peewee, soz
import os
import sys
import re
from bs4 import BeautifulSoup
from peewee import *


database = SqliteDatabase('corpwatch_tmp.db')
database.connect()
#
class Article(Model):
    Original_Filename=CharField()
    Title=CharField()
    Heading=CharField(null=True)
    NavPath=CharField(null=True) #We will want to disect this to figure out where the new location of the page should be
    #Also need to classify if it's an article or a page somewhere
    Content=TextField()


    class Meta:
        database=database

class Document():
    Original_Filename="" #Index.html/?...
    Title="" #<title>
    Heading="" #<div class=mainColHd
    NavPath="" # home >> ... >> ... from <span class="topboxtext"
    Content="" # article body from <div class="mainContent"
    UrlMapping=[] #where we want this to map to in the new site (mostly this will be guessed based on NavPath/Title i guess)
    Taxonomy=[] #i think this is like #tags


class BuildDB():
    
    #put the stuff we want from document in a Document class
    #Takes in a soup and a filename
    #Returns a Document
    def classifyDocument(self,soup,fn):
        try:
            doc=Document()
            doc.Original_Filename=fn
            doc.Content=soup.find('div',{'class':'mainContent'})
            doc.Title=soup.find('title')
            doc.NavPath=soup.find('span',{'class','topboxtext'})
            doc.Heading=soup.find('span',{'class','mainColHd'})
        except:
            sys.stderr.write(str('spamspam'))
            #sys.stderr.write(str('ClassifyDocument': "Failed to get properties from "))
            print ("Something went wrong on "+fn)
    
        return doc

    #Just grab a soup from a filename (with path)
    def getSoup(self,fn):
        #print (fn)
        with open(fn, 'r') as content_file:
            fcontent=content_file.read()
            soup = BeautifulSoup(fcontent)
            return soup
            #print fcontent

    def dumpDB(self):
        for article in Article.select(): 
            print (article.Title)

    def addRecord(self,doc):
        if not doc.Title: doc.Title="None"
        if not doc.Content:
            print("No content for file "+doc.Original_Filename+" skipping")
            return
        Article.create(
            Original_Filename=doc.Original_Filename,
            Title=doc.Title,
            Heading=doc.Heading,
            NavPath=doc.NavPath,
            Content=doc.Content
        )
        #Article.create(Original_Filename="Blah",Title="Bluh",Heading="Bloah")
        #art.save()
        

    def __init__(self):
        pth="../site-dump/www.corporatewatch.org/"
        match=r'index.*'
        matcher = re.compile(match)
        Article.create_table()
        for fn in os.listdir(pth):
            if matcher.match(fn): 
                soup=self.getSoup(pth+fn)
                mdoc=self.classifyDocument(soup,fn)
                self.addRecord(mdoc)
                #print(mdoc.Original_Filename+"\t"+mdoc.Title)
            #print(fn)
            pass
        self.dumpDB()


#ref http://stackoverflow.com/questions/6798097/find-regex-in-python-or-how-to-find-files-whose-whole-name-path-name


if __name__=="__main__":
    m=BuildDB()


#!/usr/bin/python3.2
# -*- coding: gb2312 -*-
#python3.2
import os
import sys
import re
from bs4 import BeautifulSoup
from peewee import *
database = SqliteDatabase('corpwatch_tmp.db')
database.connect()



class Article(Model):
    Original_Filename=CharField()
    Title=CharField()
    Heading=CharField(null=True)
    NavPath=CharField(null=True) #We will want to disect this to figure out where the new location of the page should be
    #Also need to classify if it's an article or a page somewhere
    Content=TextField()


    class Meta:
        database=database

class Page(Model):
    Original_Filename=CharField()
    Title=CharField()
    Heading=CharField(null=True)
    #Also need to classify if it's an article or a page somewhere
    Content=TextField()
    class Meta:
        database=database

def usage():
        print("Usage: ConstructDB.py <PATH OF WGET DUMP> <OUTPUTFILE>.sqlitedb");
        exit(1)

if __name__=='__main__': 
    if len(sys.argv)<2:usage()
    pathDump=sys.argv[1];
    pathOut=sys.argv[2];
    if not pathDump or not pathOut:
        usage()


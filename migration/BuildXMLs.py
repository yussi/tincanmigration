#!/usr/bin/python
import os
import re
from bs4 import BeautifulSoup
class Document():
    Original_Filename="" #Index.html/?...
    Title="" #<title>
    Heading="" #<div class=mainColHd
    NavPath="" # home >> ... >> ... from <span class="topboxtext"
    Content="" # article body from <div class="mainContent"
    UrlMapping=[] #where we want this to map to in the new site (mostly this will be guessed based on NavPath/Title i guess)
    Taxonomy=[] #i think this is like #tags


class BuildXML():
    
    #put the stuff we want from document in a Document class
    #Takes in a soup and a filename
    #Returns a Document
    def classifyDocument(self,soup,fn):
        doc=Document()
        doc.Original_Filename=fn
        doc.Content=soup.find('div',{'class':'mainContent'})
        doc.Title=soup.find('title').getText()
        doc.NavPath=soup.find('span',{'class','topboxtext'})
        doc.Heading=soup.find('span',{'class','mainColHd'})
        return doc

    #Just grab a soup from a filename (with path)
    def getSoup(self,fn):
        with open(fn, 'r') as content_file:
            fcontent=content_file.read()
            soup = BeautifulSoup(fcontent)
            return soup
            #print fcontent

    def __init__(self):
        pth="../site-dump/www.corporatewatch.org/"
        match=r'index.*'
        matcher = re.compile(match)
        for fn in os.listdir(pth):
            if matcher.match(fn): 
                soup=self.getSoup(pth+fn)
                mdoc=self.classifyDocument(soup,fn)
                print(mdoc.Original_Filename+"\t"+mdoc.NavPath+"\t"+mdoc.Title)
            #print(fn)
            pass


#ref http://stackoverflow.com/questions/6798097/find-regex-in-python-or-how-to-find-files-whose-whole-name-path-name


if __name__=="__main__":
    m=BuildXML()


The basic idea is, you manually wget www.corporatewatch.org
make sure it's UTF-8, python3.2 complains about it, where as 2.6 doesn't but should.
If you don't manage that, under site-dump/ there is a convert_to_utf8.sh which seemed to have done the trick for me using iconv. this should get you everything

IE:
$cd site-dump
$wget -x -r --local-encoding=UTF-8 www.corporatewatch.org
$./convert_to_utf8.sh


After you got a site dump, store it in ./site-dump/www.yourolddomain.com/index/html?lid=...
You then use the migration script BuildDB.py to generate an sqlite database, which you can open with sqlite3.
Note that this rejects pages without a <div class='mainContent'>, it will list the rejects and you'll need to make sure it doesn't miss things it shouldn't.
It will create a file called 
corpwatch_tmp.db
This has a table called article, with these columns: id|Original_Filename|Title|Heading|NavPath|Content
(Could be a bit smarter then this.)

After that there is the drupal migration module, which is at tin2drup, but isn't actually done yet. Not exactly sure how to either.

